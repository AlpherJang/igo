# igo
Golang 开发辅助工具加速基础环境搭建

[![Build Status](https://travis-ci.org/iceopen/igo.svg?branch=master)](https://travis-ci.org/iceopen/igo)
[![Go Report Card](https://goreportcard.com/badge/github.com/iceopen/igo)](https://goreportcard.com/report/github.com/iceopen/igo)

## 安装方式
> go get gitee.com/iceinto/igo

## 使用说明
```
igo golang 开发辅助工具!

Usage:
  igo [command]

Available Commands:
  beego       beego 初始化
  dep         dep 初始化
  golang   golang 初始化
  goimports   goimports 初始化
  help        Help about any command
  version     Print the version

Flags:
  -h, --help   help for igo

Use "igo [command] --help" for more information about a command.
```

## 注意事项说明
goimports 使用前需要执行 golang 命令

## 感谢
码云提供的服务
